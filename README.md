# Mocked book recommendation engine for XAI research purposes 

This repository implements the mocked book recommandation engine which is used during a xai study to determine the effect of xai on the perceived intelligence of an ai system. The app is hosted on firebase and accessible via https://ilin-xai-research.web.app/.

## Structure
- `public/`: shap diagrams for book recommendations, diagrams will be loaded dynamically based on chosen books during runtime
- `python/`: book data and linear regressor to generate shap diagrams
- `src/`: book recommendation app implemented with react, typescript, vitejs and react zustand
- firebase.json: configurations for firebase hosting
- firestore.rules: access rules for firebase firestore database
- export.js: script to export firestore data as csv

## Run locally
- Requirements: Node v17.8.0, npm 8.8.0
- `npm i`: install dependencies
- `npm run dev`: run application in dev mode
- `npm run build`: build application
- `npm run preview`: local preview of production build

## Export Firestore Data
- create service account key in firestore and save it as `serviceAccountKey.json`
- run `npm run export:surveyresults` to export and save firestore data as csv