

import { useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import { FirebaseService } from '../services/firebase'
import useBookStore from '../state/bookStore'
import './App.css'
import AppRoutes from './Routes'
import { setDoc, doc } from "firebase/firestore/lite";
import { persistError } from '../utils/helper'
import { LinearProgress } from '@mui/material'

FirebaseService.signIn().catch((e: Error) => {
  useBookStore.setState({ signInError: e })
})

const getResource = (path: string, trial: number) => {
  return (path === "/" || path === "/none") ? "timeOnSelection" + trial : ("timeOn" + path.split("/")[1] + trial)
}

const timer: Record<string, number> = {}
const trials: Record<"prev" | "now", number> = { prev: 0, now: 0 }
useBookStore.subscribe((state, previous) => {
  if (state.trial !== previous.trial) {
    trials.prev = previous.trial
    trials.now = state.trial
  }

  if (state.path === previous.path) {
    return
  }

  if (timer[previous.path]) {
    const resource = getResource(previous.path, trials.prev)
    if (trials.prev !== trials.now) {
      trials.prev = trials.now
    }

    const timeOnPath = Date.now() - timer[previous.path]
    setDoc(doc(FirebaseService.firestore, "surveyresults", state.firebaseUserId), {
      [resource]: timeOnPath
    }, { merge: true }).catch((e: Error) => {
      persistError(e, "Error writing timer")
    })
  }

  timer[state.path] = Date.now()
})

const App = () => {
  const { pathname } = useLocation()
  const { signInError, firebaseUserId, loadBooks } = useBookStore()

  useEffect(() => {
    useBookStore.setState({ path: pathname })
    loadBooks()
  }, [pathname])

  if (signInError) {
    throw signInError
  }

  if (!firebaseUserId) {
    return <LinearProgress />
  }

  return <div>
    <AppRoutes />
  </div>

}

export default App
