import { Typography } from "@mui/material"
import { story } from "../utils/story"

const End = () => {
    return <Typography display="flex" justifyContent={"center"} paddingTop="20px">
        {story.END}
    </Typography>
}

export default End