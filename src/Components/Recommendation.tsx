import { Grid, Card, CardContent, Typography, Button, CardHeader, CardActions, Divider, } from "@mui/material"
import { useNavigate } from "react-router-dom"
import useBookStore from "../state/bookStore"
import { story, getLikeProb } from "../utils/story"
import TryAgainButton from "../utils/TryAgainButton"

const Recommendation = () => {
    const { chosenBook } = useBookStore()

    return (
        <Grid container padding={2} spacing={2}>
            <Grid item xs={12}>
                <Card sx={{ padding: 2 }}>
                    <CardHeader title={"Ergebnis der Berechnungen für das Buch: " + chosenBook?.title} />
                    <CardContent>
                        <Grid container display="flex" justifyContent="space-evenly" spacing={2}>
                            <Grid item lg={3} xs={3} alignSelf="center"><Typography variant="h3" align="center">{Math.round((chosenBook?.rating ?? 0) * 100)}%</Typography></Grid>
                            <Grid item><Divider orientation="vertical" /></Grid>
                            <Grid item lg={8} xs={8} alignSelf="center">{getLikeProb(chosenBook?.rating ?? 0)}</Grid>
                            <Grid alignSelf="center" justifyContent="center" item paddingBottom={2} xs={12}><Divider /></Grid>
                        </Grid>
                    </CardContent>
                    <CardActions>
                        <TryAgainButton />
                    </CardActions>
                </Card>
            </Grid>
        </Grid >)
}

export default Recommendation