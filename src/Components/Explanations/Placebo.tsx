import { Grid, Card, CardContent, Typography, List, ListItem, CardActions, ListItemIcon, ListItemText } from "@mui/material"
import CircleIcon from '@mui/icons-material/Circle'
import useBookStore from "../../state/bookStore"
import { getPlaceboExplanation, story } from "../../utils/story"
import BookSelection from "../Home/BookSelection"
import ContinueButton from "../Home/ContinueButton"

const Placebo = () => {

    const { previousBook, selectedBooks, trial } = useBookStore()

    return <Grid container padding={2} spacing={2}>
        <Grid item xs={12}>
            <Card >
                <CardContent>
                    <Grid container spacing={4} padding={2}>
                        <Grid item xs={12}>
                            <Typography variant="h5" pb={2}>{story.EXPLANATION_HEADER + previousBook?.title}</Typography>
                            <Typography pb={2}>{story.PLACEBO_EXPLANATION_START}</Typography>
                            <List>
                                {
                                    selectedBooks.map(book =>
                                        <ListItem key={book.title}>
                                            <ListItemIcon>
                                                <CircleIcon />
                                            </ListItemIcon>
                                            <ListItemText primary={book.title} />
                                        </ListItem>)
                                }
                            </List>
                            {getPlaceboExplanation(previousBook?.rating ?? 0)}
                        </Grid>
                        <Grid item xs={12}>{trial < 2 ? story.NOT_SATISFIED_1 : story.NOT_SATISFIED_2}</Grid>
                        <Grid item xs={12}>
                            <BookSelection />
                        </Grid>
                    </Grid>
                </CardContent>
                <CardActions>
                    <ContinueButton />
                </CardActions>
            </Card>
        </Grid>
    </Grid>
}

export default Placebo