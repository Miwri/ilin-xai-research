import { Grid, Card, CardContent, Typography, CardActions, Divider } from "@mui/material"
import useBookStore from "../../state/bookStore"
import { story } from "../../utils/story"
import BookSelection from "../Home/BookSelection"
import ContinueButton from "../Home/ContinueButton"

const SHAP = () => {
    const { previousBook, trial } = useBookStore()
    return <Grid container padding={2} spacing={2}>
        <Grid item xs={12}>
            <Card >
                <CardContent>
                    <Grid container spacing={4} padding={2} display="flex" justifyContent="center">
                        <Grid item xs={12}>
                            <Typography variant="h5" paddingBottom={2}>{story.EXPLANATION_HEADER + previousBook?.title}</Typography>
                            {story.SHAP_INTRO}
                        </Grid>
                        <Grid item md={7} xs={12} display="flex" justifyContent="center">
                            <img height="100%" width="100%" src={'waterfall' + previousBook?.index + '.svg'} />
                        </Grid>
                        <Grid item xs={12}><Divider /></Grid>
                        <Grid item xs={12}>{trial < 2 ? story.NOT_SATISFIED_1 : story.NOT_SATISFIED_2}</Grid>
                        <Grid item xs={12}>
                            <BookSelection />
                        </Grid>
                    </Grid>
                </CardContent>
                <CardActions>
                    <ContinueButton />
                </CardActions>
            </Card>
        </Grid>
    </Grid>
}

export default SHAP