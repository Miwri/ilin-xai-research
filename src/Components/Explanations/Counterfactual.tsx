import { Grid, Card, CardContent, Typography, CardActions, Divider } from "@mui/material"
import useBookStore from "../../state/bookStore"
import { story } from "../../utils/story"
import { BookType } from "../../utils/types"
import BookSelection from "../Home/BookSelection"
import BookTable from "../Home/BookTable"
import ContinueButton from "../Home/ContinueButton"


const Counterfactual = () => {
    const { previousBook, trial } = useBookStore()

    return <Grid container padding={2} spacing={2}>
        <Grid item xs={12}>
            <Card >
                <CardContent>
                    <Grid container spacing={4} padding={2}>
                        <Grid item xs={12}>
                            <Typography variant="h5">{story.EXPLANATION_HEADER + previousBook?.title}</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <BookTable books={previousBook ? [previousBook] : []} onRowClicked={() => undefined} readOnly />
                        </Grid>
                        <Grid item xs={12}>
                            {previousBook?.counterfactual}
                        </Grid>
                        <Grid item xs={12}><Divider /></Grid>
                        <Grid item xs={12}>{trial < 2 ? story.NOT_SATISFIED_1 : story.NOT_SATISFIED_2}</Grid>
                        <Grid item xs={12}>
                            <BookSelection />
                        </Grid>
                    </Grid>
                </CardContent>
                <CardActions>
                    <ContinueButton />
                </CardActions>
            </Card>
        </Grid>
    </Grid>
}

export default Counterfactual