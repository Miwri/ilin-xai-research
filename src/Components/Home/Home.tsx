import { Grid, Card, CardHeader, CardContent, Typography, CardActions } from "@mui/material"
import useBookStore from "../../state/bookStore"
import { story, TRIALS, XAI_METHODS } from "../../utils/story"

import BookSelection from "./BookSelection";
import ContinueButton from "./ContinueButton";

const Home = () => {
    const { trial, selectedMethod } = useBookStore()

    return (
        <Grid container padding={2}>
            <Grid item xs={12}>
                <Card sx={{ padding: 2 }}>
                    <CardHeader title={story.HOME_TITLE} />
                    <CardContent>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <BookSelection />
                            </Grid>
                            {trial === 0 && <>
                                <Grid item xs={12}>{story.INTRO}</Grid>
                                {(selectedMethod !== XAI_METHODS[3]) && <Grid item xs={12}>{story.XAI_EXPLANATION}</Grid>}
                            </>}
                        </Grid>
                    </CardContent>
                    <CardActions sx={{ padding: 2 }}>
                        <ContinueButton />
                    </CardActions>
                </Card>
            </Grid>
        </Grid>
    )
}

export default Home