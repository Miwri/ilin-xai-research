import { Table, TableHead, TableRow, TableCell, TableBody, Checkbox } from "@mui/material";
import { BookType } from "../../utils/types"

interface HeadCell {
    disablePadding: boolean;
    id: keyof BookType;
    label: string;
    numeric: boolean;
}

const headCells: readonly HeadCell[] = [
    {
        id: 'title',
        numeric: false,
        disablePadding: true,
        label: 'Titel',
    },
    {
        id: 'author',
        numeric: false,
        disablePadding: false,
        label: 'Autor',
    },
    {
        id: 'publisher',
        numeric: false,
        disablePadding: false,
        label: 'Verlag',
    },
    {
        id: 'pages',
        numeric: true,
        disablePadding: false,
        label: 'Seitenzahl',
    },
    {
        id: 'year',
        numeric: true,
        disablePadding: false,
        label: 'Erscheinungsjahr',
    },
];

const BookTable = (props: { books: BookType[], onRowClicked: (row: BookType) => void, chosenBook?: BookType, readOnly?: Boolean }) =>
    <div style={{ overflow: 'auto', maxWidth: '100%' }}>
        <Table>
            <TableHead>
                <TableRow>
                    {!props.readOnly &&
                        <TableCell />
                    }
                    {headCells.map((headCell) => (
                        <TableCell
                            key={headCell.id}
                            align={headCell.numeric ? 'left' : 'left'}
                            padding={headCell.disablePadding ? 'none' : 'normal'}
                        >
                            {headCell.label}
                        </TableCell>
                    ))}
                </TableRow>
            </TableHead>
            <TableBody>
                {props.books
                    .map((row, index) => {
                        const isItemSelected = row.title === props.chosenBook?.title;
                        const labelId = `enhanced-table-checkbox-${index}`;

                        return (
                            <TableRow
                                hover={!props.readOnly}
                                onClick={() => props.onRowClicked(row)}
                                role={props.readOnly ? undefined : "checkbox"}
                                aria-checked={isItemSelected}
                                tabIndex={-1}
                                key={row.title}
                                selected={isItemSelected}
                            >
                                {!props.readOnly && <TableCell padding="checkbox">
                                    <Checkbox
                                        color="primary"
                                        checked={isItemSelected}
                                        inputProps={{
                                            'aria-labelledby': labelId,
                                        }}
                                    />
                                </TableCell>}
                                <TableCell
                                    component="th"
                                    id={labelId}
                                    scope="row"
                                    align="left"
                                    padding="none"
                                >
                                    {row.title}
                                </TableCell>
                                <TableCell align="left">{row.author}</TableCell>
                                <TableCell align="left">{row.publisher}</TableCell>
                                <TableCell align="left">{row.pages}</TableCell>
                                <TableCell align="left">{row.year}</TableCell>
                            </TableRow>
                        );
                    })}
            </TableBody>
        </Table>
    </div>

export default BookTable