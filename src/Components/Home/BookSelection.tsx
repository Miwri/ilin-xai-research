import { Typography, Table, TableHead, TableRow, TableCell, TableBody, Checkbox } from "@mui/material";
import { useEffect, useState } from "react";
import useBookStore from "../../state/bookStore";
import { story, getBooks } from "../../utils/story";
import { BookType } from "../../utils/types";
import BookTable from "./BookTable";

const BookSelection = () => {

    const { chosenBook, setChosenBook, trial } = useBookStore()
    const [currentBooks, setCurrentBooks] = useState<BookType[]>([])

    useEffect(() => {
        setCurrentBooks(getBooks()[trial])
    }, [trial])

    return <>
        {(trial === 0) && <Typography>{story.SZENARIO_INTRO}</Typography>}
        <Typography variant="body1" fontWeight="bold">{story.WISH_BOOK_SZENARIO_2}</Typography>
        <BookTable books={currentBooks} onRowClicked={setChosenBook} chosenBook={chosenBook} />
    </>
}

export default BookSelection