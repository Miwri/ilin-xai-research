import { Grid, Button } from "@mui/material"
import { setDoc, doc } from "firebase/firestore/lite"
import { FirebaseService } from "../../services/firebase"
import { persistError } from "../../utils/helper"
import { story } from "../../utils/story"
import { useNavigate } from "react-router-dom"
import useBookStore from "../../state/bookStore"


const ContinueButton = () => {
    const { chosenBook, trial, firebaseUserId } = useBookStore()
    const navigate = useNavigate()


    const addBook = () => {
        const bookName = "chosenBook" + trial
        const { counterfactual, ...bookToPersist } = chosenBook ?? {}
        setDoc(doc(FirebaseService.firestore, "surveyresults", firebaseUserId), {
            [bookName]: bookToPersist,
        }, { merge: true }).catch((e: Error) => {
            persistError(e, "Error writing book")
        })
    }

    const continueButtonClicked = () => {
        navigate("/recommendation", { replace: true })
        addBook()
    }

    return <Grid container spacing={2} padding={2}>
        <Grid item xs={12}>
            <Button disabled={!chosenBook} fullWidth variant="contained" onClick={continueButtonClicked}>{story.CONTINUE}</Button>
        </Grid>
    </Grid>
}

export default ContinueButton