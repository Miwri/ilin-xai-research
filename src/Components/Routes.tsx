import { LinearProgress } from "@mui/material"
import { lazy, Suspense } from "react"
import { Route, Routes } from "react-router-dom"
import ErrorFallback from "./ErrorFallback"

const Home = lazy(() => import("./Home/Home"))
const Recommendation = lazy(() => import("./Recommendation"))
const SHAP = lazy(() => import("./Explanations/Shap"))
const Counterfactual = lazy(() => import("./Explanations/Counterfactual"))
const Placebo = lazy(() => import("./Explanations/Placebo"))
const End = lazy(() => import("./End"))

const AppRoutes = () =>
    <Routes>
        <Route path="/" element={<Suspense fallback={<LinearProgress />}><Home /></Suspense>} />
        <Route path="/none" element={<Suspense fallback={<LinearProgress />}><Home /></Suspense>} />
        <Route path="/recommendation" element={<Suspense fallback={<LinearProgress />}><Recommendation /></Suspense>} />
        <Route path="/shap" element={<Suspense fallback={<LinearProgress />}><SHAP /></Suspense>} />
        <Route path="/counterfactual" element={<Suspense fallback={<LinearProgress />}><Counterfactual /></Suspense>} />
        <Route path="/placebo" element={<Suspense fallback={<LinearProgress />}><Placebo /></Suspense>} />
        <Route path="/end" element={<Suspense fallback={<LinearProgress />}><End /></Suspense>} />
        <Route path="/error" element={<Suspense fallback={<LinearProgress />}><ErrorFallback /></Suspense>} />
    </Routes>

export default AppRoutes