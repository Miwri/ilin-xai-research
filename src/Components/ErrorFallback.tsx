import { Typography } from "@mui/material"
import WarningIcon from '@mui/icons-material/Warning';

const ErrorFallback = (error: Object) => {
    return <>
        <Typography display="flex" justifyContent={"center"} paddingTop="20px">
            <WarningIcon sx={{ height: 200, color: "darkred", width: 200 }} />
        </Typography>
        <Typography display="flex" justifyContent={"center"} padding="20px" variant="body1">
            Es scheint, als wäre ein Fehler in unserer Anwendung aufgetreten. Bitte versuchen Sie, die Seite erneut zu laden. Sollte das den Fehler nicht beheben, löschen Sie den Cache und die Websitedaten oder versuchen Sie, die Seite mit einem anderen Browser aufzurufen. Vielen Dank!
        </Typography>
    </>
}

export default ErrorFallback