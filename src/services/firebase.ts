import { initializeApp } from "firebase/app";
import { doc, getFirestore, setDoc, Timestamp } from "firebase/firestore/lite";
import { getAuth, signInAnonymously, onAuthStateChanged } from "firebase/auth"
import useBookStore from "../state/bookStore";
import { XAI_METHODS } from "../utils/story";
import { persistError } from "../utils/helper";


const firebaseConfig = {
  apiKey: "AIzaSyBVV78XseODvq-pDYXY5ngPF1EPsFImmJI",
  authDomain: "ilin-xai-research.firebaseapp.com",
  projectId: "ilin-xai-research",
  storageBucket: "ilin-xai-research.appspot.com",
  messagingSenderId: "420485142896",
  appId: "1:420485142896:web:4c2b9e85122b9d426817b5",
  measurementId: "G-ZX84DC421S"
}

const app = initializeApp(firebaseConfig)
const auth = getAuth()
const firestore = getFirestore(app)

onAuthStateChanged(auth, authState => {
  if (authState?.uid) {
    const searchParams = new URLSearchParams(window.location.search)
    const xai_method_param = parseInt(searchParams.get('xai') ?? '1')
    const xai_method_key = isNaN(xai_method_param) ? 1 : xai_method_param

    useBookStore.setState({ firebaseUserId: authState.uid, selectedMethod: XAI_METHODS[xai_method_key - 1] })

    setDoc(doc(FirebaseService.firestore, "surveyresults", authState.uid), {
      createdDate: Timestamp.now(),
      panelUserId: searchParams.get('userId')
    }, { merge: true }).catch((e: Error) => {
      persistError(e, "Error writing initial document")
    })
  }
})



export const FirebaseService = {
  firestore,
  signIn: () => signInAnonymously(auth)
}