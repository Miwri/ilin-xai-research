import { Typography } from "@mui/material"
import useBookStore from "../state/bookStore"
import { fisherYatesShuffle } from "./helper"

export const story = {
    INTRO: <Typography>Das KI-System kann nun die Wahrscheinlichkeit berechnen, dass Ihrem Freund das ausgewählte Buch gefallen wird. Dazu nutzt es die bisher gelesenen Bücher Ihres Freundes, die Sie zuvor bereits eingegeben haben. </Typography>,

    XAI_EXPLANATION: <Typography> Im Anschluss an die Bewertung können Sie sich die <b>Ergebnisse erklären lassen.</b> </Typography>,

    HOME_TITLE: `Auswahl des Buchgeschenks`,

    TRY_AGAIN_BUTTON: `Zurück zur Buchauswahl`,

    EXPLANATION_HEADER: "Erklärung zur berechneten Wahrscheinlichkeit für das Buch: ",

    SZENARIO_INTRO: "Sie befinden sich nun in der Buchhandlung und betrachten die folgenden Bücher, die sich gerade im Angebot befinden.",

    WISH_BOOK_SZENARIO_2: "Bitte wählen Sie aus, welches Buch Sie Ihrem Freund gerne schenken würden:",

    NOT_SATISFIED_1: <Typography>So ganz zufrieden sind Sie mit dem Ergebnis noch nicht. Daher gehen Sie zum nächsten Regal des Buchladens, um sich <b>ein anderes Buch auszusuchen.</b></Typography>,

    SZENARIO_TWO_TRIAL_TWO: <Typography>Da der erste Versuch leider überhaupt nicht den Geschmack Ihres Freundes getroffen hat, schauen Sie sich in einer anderen Ecke des Buchladens nach einer Alternative um. Als Hilfestellung schauen Sie selbst noch einmal auf die Liste der Bücher, die Ihr Freund bereits gelesen hat (siehe oben).</Typography>,

    NOT_SATISFIED_2: <Typography>Da das Ergebnis noch nicht ganz Ihren Erwartungen entspricht, starten Sie einen letzten Versuch.</Typography>,

    SHAP_INTRO: <Typography>Im folgenden Diagramm sehen Sie, wie sich die <b>Eigenschaften des gewählten Buches</b> auf die Berechnung der Wahrscheinlichkeit ausgewirkt haben. <br />
        E(f(x)) unten am Diagramm zeigt einen <b>Basiswert</b> für die Wahrscheinlichkeit, dass Ihrem Freund ein <b>beliebiges Buch</b> im Sortiment der Buchhandlung gefällt.<br />
        f(x) oben am Diagramm zeigt die <b>errechnete Wahrscheinlichkeit</b> dafür, dass das <b>gewählte Buch</b> Ihrem Freund gefällt. <br />
        Das Diagramm ist von unten nach oben zu lesen. Die <b>Länge der Pfeile</b> zeigt, <b>wie stark</b> ein Kriterium die Berechnung der Wahrscheinlichkeit beeinflusst hat.
        Die <b>Richtung und Farbe der Pfeile</b> zeigt, ob die Wahrscheinlichkeit <b>positiv (rot)</b> oder <b>negativ (blau)</b> beeinflusst wurde. <br />
        Die Eigenschaften sind <b>aufsteigend</b> nach der Stärke der Beeinflussung sortiert.
        Die Nachkommazahlen stehen für Prozentwerte, d.h. 0.69 bedeutet bspw. eine Wahrscheinlichkeit von 69%. <br />
    </Typography>,

    COUNTERFACTUAL_INTRO: <Typography>Im Folgenden wird Ihnen angezeigt, was passiert wäre, wenn Sie eine andere Auswahl getroffen hätten.</Typography>,

    COUNTERFACTUAL_EXPLANATION_START: "Mögliche Veränderung des Ergebnis",

    PLACEBO_EXPLANATION_START: "Ihr Freund hat bereits folgende Bücher gelesen: ",

    EXPLANATION_EXTENSION: <Typography>Sie wollen wissen, wie das KI-System zu dieser Einschätzung kam. <br /> Daher lassen Sie sich über folgenden Button eine Erklärung anzeigen.</Typography>,

    CONTINUE: "berechnung starten",

    XAI_RECOMM_BUTTON: "Wie kamen diese Ergebnisse zustande?",

    DONE: <Typography>Da Sie nicht länger suchen möchten, entscheiden Sie sich für dieses Buch. Erfreut darüber, ein passendes Buch gefunden zu haben, verlassen Sie die Buchhandlung.</Typography>,

    LEAVE: "KI-System verlassen",

    END: "Vielen Dank für die Nutzung unseres KI-Systems. Bitte fahren Sie nun mit der Bearbeitung des Fragebogens fort.",
}

export const getCounterfactualSecond = (values: { property: String, val1: string, val2: string }[], cat1: string, cat2: string) => {
    const explanationFragments: String[] = []
    values.forEach(value => {
        explanationFragments.push(`<b>${value.property}</b> des Buches <b>${value.val1}</b> `)
    })

    const explanation = 'Weil ' + explanationFragments.reduce((prev, curr) => prev + ' und ' + curr) + 'ist, ist es <b>' + cat1 + '</b>, dass Ihrem Freund das ausgewählte Buch gefällt.'

    const recommendation = 'Wenn <b>' + values[0]?.property + values[0]?.val2 + '</b>' + (values[1] ? 'und <b>' + values[1]?.property + (values[1]?.val2) : '') + '</b> wäre, würde Ihrem Freund das Buch <b>' + cat2 + '</b> gefallen.'

    return <>
        <Typography dangerouslySetInnerHTML={{
            __html: explanation
        }} />
        <Typography dangerouslySetInnerHTML={{
            __html: recommendation
        }} />
    </>
}

export const getPlaceboExplanation = (percentage: number) => <Typography pt={2}> Aufgrund dieser Eingaben wurde das Ergebnis <b>{Math.ceil((percentage) * 100)}%</b> berechnet.</Typography>

export const getLikeProb = (percentage: number) =>
    percentage > 0.9 ? <Typography>Basierend auf den bereits gelesenen Büchern ist es <b> sehr wahrscheinlich</b>, dass sich Ihr Freund über das ausgewählte Buch freuen würde.</Typography>
        : percentage > 0.79 ? <Typography>Basierend auf den bereits gelesenen Büchern ist es <b>wahrscheinlich</b>, dass sich Ihr Freund über das ausgewählte Buch freuen würde.</Typography>
            : percentage > 0.5 ? <Typography>Basierend auf den bereits gelesenen Büchern ist es <b>möglich</b>, dass sich Ihr Freund über das ausgewählte Buch freuen würde.</Typography>
                : <Typography>Basierend auf den bereits gelesenen Büchern ist es <b>eher unwahrscheinlich</b>, dass sich Ihr Freund über das ausgewählte Buch freuen würde.</Typography>

export const XAI_METHODS = [
    "shap",
    "counterfactual",
    "placebo",
    "none",
]

export const TRIALS = [
    story.INTRO,
    story.SZENARIO_TWO_TRIAL_TWO,
    "",
]

export const getBooks = () => [
    fisherYatesShuffle(useBookStore.getState().allBooks.slice(5, 10)),
    fisherYatesShuffle(useBookStore.getState().allBooks.slice(10, 15)),
    fisherYatesShuffle(useBookStore.getState().allBooks.slice(15,)),
]