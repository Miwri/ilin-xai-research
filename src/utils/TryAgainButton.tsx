import { Grid, Button, Typography } from "@mui/material"
import { useNavigate } from "react-router-dom"
import useBookStore from "../state/bookStore"
import { story } from "./story"

const TryAgainButton = () => {
    const navigate = useNavigate()
    const { incrementTrial, setChosenBook, trial, chosenBook, setPreviousBook, selectedMethod } = useBookStore()

    const tryAgainButtonClicked = () => {
        setPreviousBook(chosenBook)
        setChosenBook(undefined)
        if (trial < 2) {
            navigate(`/${selectedMethod}`, { replace: true })
        }
        else {
            navigate("/end", { replace: true })
            window.parent.postMessage("done", "*")
        }
        incrementTrial()
    }

    return <Grid container spacing={2} padding={2}>
        {trial > 1 ? <Grid item xs={12}>{story.DONE}</Grid>
            : selectedMethod === 'none' ? <Grid item xs={12}>{trial < 1 ? story.NOT_SATISFIED_1 : story.NOT_SATISFIED_2}</Grid>
                : <Grid item xs={12}> {story.EXPLANATION_EXTENSION} </Grid>
        }
        <Grid item xs={12}>
            <Button variant="contained" fullWidth onClick={tryAgainButtonClicked} >{trial > 1 ? story.LEAVE : selectedMethod === 'none' ? story.TRY_AGAIN_BUTTON : story.XAI_RECOMM_BUTTON}</Button>
        </Grid>

    </Grid>


}

export default TryAgainButton