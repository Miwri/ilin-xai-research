import { collection, addDoc, Timestamp } from "firebase/firestore/lite"
import StackTrace from "stacktrace-js"
import { FirebaseService } from "../services/firebase"

export const persistError = async (error: Error, innerMessage?: string) => {
    console.error("app error", error)
    const deMinifiedStack = await StackTrace.fromError(error)
    addDoc(collection(FirebaseService.firestore, "/errors"), {
        date: Timestamp.now(),
        message: error.message,
        name: error.name,
        innerMessage: innerMessage ?? "unknown",
        stack: deMinifiedStack.toString(),
    })
}

// https://dev.to/codebubb/how-to-shuffle-an-array-in-javascript-2ikj
export const fisherYatesShuffle = <T>(array: T[]): T[] => {
    const shuffleArray = [...array]
    for (let i = shuffleArray.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = shuffleArray[i];
        shuffleArray[i] = shuffleArray[j];
        shuffleArray[j] = temp;
    }
    return shuffleArray
}
