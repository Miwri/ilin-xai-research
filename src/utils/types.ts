export type BookType = {
    index: number,
    title: string,
    author: string,
    publisher: string,
    year: number,
    pages: number,
    genre?: string,
    subgenre?: string,
    rating: number,
    counterfactual?: JSX.Element,
}