import create from "zustand"
import { getCounterfactualSecond, } from "../utils/story"
import { BookType } from "../utils/types"
//@ts-expect-error
import books from "./../books.csv"

interface BookState {
  chosenBook: BookType | undefined,
  previousBook: BookType | undefined,
  allBooks: BookType[],
  selectedBooks: BookType[],
  selectedMethod: string,
  trial: number,
  firebaseUserId: string,
  signInError?: Error,
  path: string,
  setChosenBook: (book?: BookType) => void,
  setPreviousBook: (book?: BookType) => void,
  incrementTrial: () => void,
  loadBooks: () => void,
}

const useBookStore = create<BookState>(set => ({
  previousBook: undefined,
  allBooks: [],
  chosenBook: undefined,
  selectedBooks: [],
  selectedMethod: "shap",
  signInError: undefined,
  trial: 0,
  firebaseUserId: "",
  path: "",
  setChosenBook: (book => set(() => ({ chosenBook: book }))),
  setPreviousBook: (book => set(() => ({ previousBook: book }))),
  incrementTrial: () => set(state => {
    if (state.trial < 2) {
      return ({ trial: state.trial + 1 })
    }
    else {
      return ({ trial: 0 })
    }
  }),
  loadBooks: () => {
    const importedBooks: BookType[] = books
    importedBooks.forEach((book, index) => {
      book.index = index
      if (index > 4)
        book.counterfactual = counterfactuals[index - 5]
    })

    return set(() => ({
      selectedBooks: importedBooks.slice(0, 5),
      allBooks: importedBooks,
    }))
  },
}))

export default useBookStore

const counterfactuals: JSX.Element[] = [
  getCounterfactualSecond([{ property: 'das Genre', val1: 'realistische Erzählung', val2: ' ein fiktiver Roman ' }, { property: 'die Seitenzahl', val1: '1120', val2: ' unter 500 ' }], 'eher unwahrscheinlich', 'wahrscheinlich'),
  getCounterfactualSecond([{ property: 'das Genre', val1: 'Sachbuch', val2: ' ein fiktiver Roman ' }, { property: 'die Seitenzahl', val1: '624', val2: ' unter 500 ' }], 'eher unwahrscheinlich', 'wahrscheinlich'),
  getCounterfactualSecond([{ property: 'das Genre', val1: 'Sachbuch', val2: ' ein fiktiver Roman ' }, { property: 'die Seitenzahl', val1: '800', val2: ' unter 500 ' }], 'eher unwahrscheinlich', 'wahrscheinlich'),
  getCounterfactualSecond([{ property: 'das Genre', val1: 'realistische Erzählung', val2: ' ein fiktiver Roman ' }, { property: 'das Alter', val1: '129 Jahre', val2: ' unter 30 ' }], 'eher unwahrscheinlich', 'wahrscheinlich'),
  getCounterfactualSecond([{ property: 'das Genre', val1: 'realistische Erzählung', val2: ' ein fiktiver Roman ' }, { property: 'das Alter', val1: '121 Jahre', val2: ' unter 30 ' }], 'eher unwahrscheinlich', 'wahrscheinlich'),
  getCounterfactualSecond([{ property: 'das Genre', val1: 'Sachbuch', val2: ' ein fiktiver Roman ' }, { property: 'das Thema', val1: 'Gesellschaft', val2: ' Fantasy ' }], 'eher unwahrscheinlich', 'sehr wahrscheinlich'),
  getCounterfactualSecond([{ property: 'das Genre', val1: 'Sachbuch', val2: ' ein fiktiver Roman ' }, { property: 'das Thema', val1: 'Gesellschaft', val2: ' Fantasy ' }], 'eher unwahrscheinlich', 'sehr wahrscheinlich'),
  getCounterfactualSecond([{ property: 'die Seitenzahl', val1: '864', val2: ' unter 500 ' }, { property: 'das Thema', val1: ' Historisch', val2: ' Fantasy ' }], 'möglich', 'sehr wahrscheinlich'),
  getCounterfactualSecond([{ property: 'die Seitenzahl', val1: '800', val2: ' unter 500 ' }, { property: 'das Alter', val1: '56 Jahre', val2: ' unter 30 ' }], 'wahrscheinlich', 'sehr wahrscheinlich'),
  getCounterfactualSecond([{ property: 'das Alter', val1: '56 Jahre', val2: ' unter 30 ' }], 'wahrscheinlich', 'sehr wahrscheinlich'),
]