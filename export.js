
const admin = require("firebase-admin");
const fs = require("fs")

const serviceAccount = require("./serviceAccountKey.json");
const app = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});
const firestore = admin.firestore(app)

const parseDocument = (doc) => {
    const { createdDate, ...data } = doc.data()

    const jsonData = {
        ...data,
        createdDate: new admin.firestore.Timestamp(createdDate.seconds, createdDate.nanoseconds).toDate().toISOString()
    }
    const { chosenBook0, chosenBook1, chosenBook2, ...metadata } = jsonData

    return { ...metadata, book0: chosenBook0?.title, book1: chosenBook1?.title, book2: chosenBook2?.title }
}

const errorCallback = (error) => {
    if (error) {
        errorCallback(error)
    }
}
firestore.collection("surveyresults").get().then(snapshot => {
    const docdata = snapshot.docs.map(parseDocument)
    const sep = ";"
    const fileName = __dirname + "/results" + new Date().toISOString() + ".csv"
    const titleArray = [
        "panelUserId",
        "createdDate",
        "book0",
        "book1",
        "book2",
        "timeOnSelection0",
        "timeOnSelection1",
        "timeOnSelection2",
        "timeOnrecommendation0",
        "timeOnrecommendation1",
        "timeOnrecommendation2",
        "timeOncounterfactual1",
        "timeOncounterfactual2",
        "timeOnplacebo1",
        "timeOnplacebo2",
        "timeOnshap1",
        "timeOnshap2",
    ]

    const fileWriter = fs.createWriteStream(fileName, { flags: 'a' })

    for (const title of titleArray) {
        fileWriter.write(title + sep, errorCallback)
    }
    fileWriter.write("\n", errorCallback)
    for (const data of docdata) {
        for (const title of titleArray) {
            fileWriter.write(data[title] ? (data[title] + sep) : sep, errorCallback)
        }
        fileWriter.write("\n", errorCallback)
    }
})
